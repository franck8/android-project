package fr.toulouse.greta.formation.zooapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;


public class HomeActivity extends Activity {

    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.home);
        TextView tvNews = findViewById(R.id.tv_news);
        /**tvNew.setText("pas de news");*/
        try {
            InputStream is = getAssets().open("news.txt");
            InputStreamReader lecture = new InputStreamReader(is);
            BufferedReader buff = new BufferedReader(lecture);
            String l, t = "";
            while ((l = buff.readLine()) != null) {
                t += l + "\n";
            }
            buff.close();
            tvNews.setText(t);

        } catch (
                IOException ex) {
            Log.e("name", "error", ex);

        }
        Button btMap = findViewById(R.id.bt_map);
        btMap.setOnClickListener(new View.OnClickListener() {
            /**
             * utilisation d'une classe anonyme
             */
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MapActivity.class);
                /** on se place dans le contexte de HomeActivity*/
                startActivity(i);
            }

        });
        Button btAlert = findViewById(R.id.bt_alert);
        btAlert.setOnClickListener(new View.OnClickListener() {
            /**
             * utilisation d'une classe anonyme
             */
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, AlertActivity.class);
                /** on se place dans le contexte de AlertActivity*/
                startActivity(i);

            }
        });
        Button btAnnuaire = findViewById(R.id.bt_annuaire);
        btAnnuaire.setOnClickListener(new View.OnClickListener() {
            /**
             * utilisation d'une classe anonyme
             */
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, Annuaire.class);
                /** on se place dans le contexte de AlertActivity*/
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu); /**lit le fichier xml*/
        MenuItem mi = menu.findItem(R.id.item3);
        SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
        mi.setChecked(sp.getBoolean("save", false));
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Intent i = new Intent(HomeActivity.this, MapActivity.class);
                startActivity(i);
                return true;
            case R.id.item3:
                item.setChecked(!item.isChecked());
                SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
                SharedPreferences.Editor e = sp.edit();
                /**pourquoi passer par un editeur ?
                 * l'unicite evite l'entrelacement de modifications en paralelle,
                 * et permet donc de s'assurer de l'atomiciter de la transaction
                 *
                 * e peu etre
                 * un int, short, byte, long, double, float
                 * P[]
                 * String
                 * String[]
                 * Serializable
                 * Parcelable
                 * bundle (un dico en faite, si donnees complexes ex: Bean)
                 * si aggregat d'objet attention de voir quel type d'objet sont aggrege les uns aux autres
                 * ex: un service ne pourra pas etre contenu dans un bindle)
                 *
                 * */
                e.putBoolean("save", item.isChecked());
                e.commit();
            case R.id.item4:
                Log.i("home", "Recherche: Lamantin");
                final Thread t = new Thread() {
                    public void run()
                    {
                        try {
                            URL url = new URL("https://fr.wikipedia.org/api/rest_v1/page/summary/Lamantin");
                            InputStream is = url.openConnection().getInputStream();
                            BufferedReader br = new BufferedReader(new InputStreamReader(is));
                            String s = br.readLine();
                            is.close();
                            JSONObject o1 = new JSONObject(s);
                            String extract = o1.getString("extract");
                            Log.i("home", "extract");
                            Toast.makeText(HomeActivity.this, extract, Toast.LENGTH_LONG).show();
                            /** probleme ici une thread ne peut avoir acces a l'ecran,
                             * en effet on ne peut pas lancer plusieurs treads sur le meme ecran
                             * UN SEUL THREAD A LE DROIT A L'ACCES A L'AFFICHAGE.
                             * 2 solutions possibles specifique a Android
                             * 1 / utilisation d'Asynctask
                             * 2 / handler
                             * */
                        } catch (Exception ex) {
                            Log.i("home", "!", ex);
                        }
                    }


                };
                t.start();



    }
        return true;
    }
}