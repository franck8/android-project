package fr.toulouse.greta.formation.zooapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;

import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileWriter;

public class PopcornActivity extends Activity {
    private long start;

    protected void onCreate(Bundle b) {
        super.onCreate(b);
        //setContentView(new PopcornView(this));
        start = System.currentTimeMillis();
        long t = getIntent().getLongExtra("timer", 0);
        if (t > 2000) {
            String animals[] = getResources().getStringArray(R.array.animals);
            String f = animals[(int) (Math.random() * animals.length)];
            String txt = getString(R.string.avertissement, f, t / 1000.0);
            /**getString est un delegue (cf. Design Pattern)*/
            Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
            trylog();
        }

    }

    private void trylog() {
/**
 * 3 cas
 permission accorde
 permission pas encore demandée
 permission demandée
 */
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            log();

        if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            log();
        }
    }

    private void log() {
        Log.i("popcorn", "leg");
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            try {
                FileWriter fw = new FileWriter(new File(dir, "pop.log"), true);
                fw.write("pop!\n");
                fw.close();
            } catch (Exception ex) {
                Log.i("error","error !");
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra(AquariumActivity2.RESULT_KEY, System.currentTimeMillis() - start);
        setResult(0, i);
        super.onBackPressed();
        /**   le super.onBackPressed() stop la methode ici */
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, MapActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            /**
             * le clear_top evite le cumul d'activites sur la pile
             * */
            startActivity(i);

        }
        return true;
    }

    private class PopcornView extends View {

        // ce que j'affiche est la view

        public PopcornView(Context context) {
            super(context);
        }

        public void onDraw(Canvas c) {
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.popcorn);
            c.drawBitmap(b, 0, 0, null);
        }


    }

}