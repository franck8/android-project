package fr.toulouse.greta.formation.zooapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;


public class Annuaire extends Activity {

    protected void onCreate(Bundle b) {

        super.onCreate(b);
        TableLayout t;
        setContentView(R.layout.annuaire);
        t = findViewById(R.id.tableLayout);


        for (int l = 0; l <t.getChildCount(); l++){
            TableRow r = (TableRow)t.getChildAt(l);
            for (int c = 0; c < r.getChildCount(); c++) {
                r.getChildAt(c).setBackgroundColor(Color.rgb(0, 0, ((l % 2) + (c % 2)) * 60));


            }
        }

    }


}
