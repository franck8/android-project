package fr.toulouse.greta.formation.zooapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;


public class AquariumActivity2 extends Activity {
    private long start;
    public static final String RESULT_KEY = "t2";

    /**
     * activity est un controller
     */
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(new AquariumView(this));
        /**
         * i pour info
         * Event Log en bas a droite de l'ecran (LogCat)
         */
        Log.i("MapActivity", "onCreate()");
        start = System.currentTimeMillis();
    }

    public boolean onTouchEvent(MotionEvent event) {
        long timer = System.currentTimeMillis() - start;
        Log.i("Aquarium", +timer + "ms");
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, PopcornActivity.class);
            i.putExtra("timer", timer); /** permet la transmission d'infos -  cf. popcornActivity*/
            startActivityForResult(i, 0);
            //startActivity(i);

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        long t2 = data.getLongExtra(RESULT_KEY, 0);
        start += t2;
        /**
         * permet d'afficher le toast apres les 2 secondes rester sur aquarium
         * */
    }

    /**
     * La vue connait la MAP et inversement
     */
    private class AquariumView extends View {

        // ce que j'affiche est la view

        public AquariumView(Context context) {
            super(context);
        }

        /**
         * afichage de l'image dans l'app
         */
        @Override
        public void onDraw(Canvas c) {
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.aquarium2);
            c.drawBitmap(b, 0, 0, null);
        }
    }


}



