package fr.toulouse.greta.formation.zooapp;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ZooHelper extends SQLiteOpenHelper {

    /**
     * comme la classe mere n'a pas de constructeur par default
     * il faut donc en creer un ici obligatoirement
     */

    public ZooHelper(Context ctx) {
        super(ctx, "zoo.db", null, 1);
        /** la structure de la base est fonction de la version du constructeur*/
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /**Connection a la BD*/
        db.execSQL("CREATE TABLE problems(id INT PRIMARY KEY, title TEXT, location TEXT, description TEXT, urgent BOOLEAN)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    public String insertAlert (String title, String location, String description,boolean urgent){

        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO problems(title, location, description, urgent) " +
                        "VALUES (?,?,?,?)", new Object[]{title,location,description,urgent});
        Cursor c = db.rawQuery("SELECT title FROM problems WHERE location=?", new String []{location});
        String res = "";
        while (c.moveToNext()) {
          res += c.getString(0)+"\n";
        }
        db.close();
        return res;
    }
}
