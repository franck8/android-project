package fr.toulouse.greta.formation.zooapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class AlertActivity extends Activity {

    private EditText etTitle, etDescription;
    private AutoCompleteTextView etLieu;
    /**
     * declaration en AutoCompleteTextView pour le lieu
     */
    private CheckBox check;
    private Button button;


    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.alert);
        etTitle = findViewById(R.id.titreEditText);
        etLieu = findViewById(R.id.lieuEditText);
        etDescription = findViewById(R.id.descriptionEditText);
        check = findViewById(R.id.checkBox);
        button = findViewById(R.id.envoyerButton);

        String[] locations = getResources().getStringArray(R.array.alert_location);
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, locations);
        /**======================================================================================*/
        /** on contruit l'adaptateur qui lie la liste et le layout dans un contexte precis*/
        etLieu.setAdapter(aa);
    }

    public void send(View v) {
        String exclam = "";
        if (check.isChecked()) {
            exclam = getString(R.string.alert_toast_excl, etTitle.getText());
        } else
            exclam = getString(R.string.alert_toast, etTitle.getText());

        Toast.makeText(this, exclam, Toast.LENGTH_LONG).show();
        finish();
        /**on veut lors du clic
         * le mot "envoyer"
         * on veut ! si urgent
         * fermer la fenetre
         * */
        /**==========================================================================================*/
        ZooHelper zh = new ZooHelper(this);
        zh.insertAlert(etTitle.getText().toString(),
                etLieu.getText().toString(),
                etDescription.getText().toString(),
                check.isChecked());
    }
        public void ask (final View v){
            AlertDialog.Builder ab = new AlertDialog.Builder(this);
            /**builder sous classe de la classe AlertDialog*/
            ab.setTitle(R.string.alert);
            ab.setMessage(R.string.alertConfirm);

/**creation d'une boite de dialogue avec 2 boutons*/
            ab.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    /**finish();*/
                }
            });
            ab.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    send(v);
                }
            });
            AlertDialog d = ab.create();
            d.show();
/**================================================================================================*/
        }


    }
