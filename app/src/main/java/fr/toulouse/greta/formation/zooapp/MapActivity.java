package fr.toulouse.greta.formation.zooapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class MapActivity extends Activity {
    /**
     * activity est un controller
     */
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(new MapView(this));
        /**
         * i pour info
         * Event Log en bas a droite de l'ecran (LogCat)
         */
        Log.i("MapActivity", "onCreate()");
        Toast.makeText(this, "Bienvenue", Toast.LENGTH_LONG).show();

    }

    /**
     * event de toucher
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, AquariumActivity2.class);
            startActivity(i);
        }
        return true;

        /**possibilite de retirer le super avant onTouchEvent(event) qui lui meme peut etre oter*/
        /**quand on click un ontouchEvent est creer et qd on relache aussi donc double clique pour revenir en arriere
         * le if permet d'avoir acces a la map aquarium et ne permet pas de retrouner en arriere
         * */
    }

    /**
     * La vue connait la MAP et inversement
     */
    private class MapView extends View {

        // ce que j'affiche est la view

        public MapView(Context context) {
            super(context);
        }

        /**
         * afichage de l'image dans l'app
         */
        @Override
        public void onDraw(Canvas c) {
            Bitmap b = ((BitmapDrawable)getResources().getDrawable(R.drawable.mapzoo1,null)).getBitmap();
            c.drawBitmap(b, 0, 0, null);
        }
    }

    public void onBackPressed() {
        Intent i = new Intent(getIntent().ACTION_VIEW);
        i.setData(Uri.parse("https://twycrosszoo.org/wp-content/uploads/2019/04/Easter-2019-FLAT-MAP-Front.jpg"));
        startActivity(i);
        super.onBackPressed();

    }
}
